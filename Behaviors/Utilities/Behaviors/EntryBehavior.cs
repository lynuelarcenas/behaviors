﻿using System;
using Xamarin.Forms;

namespace Behaviors.Utilities.Behaviors
{
    public class EntryBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Unfocused += OnEntryUnfocused;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.Unfocused -= OnEntryUnfocused;
            base.OnDetachingFrom(entry);
        }

        void OnEntryUnfocused(object sender, EventArgs e)
        {
            var entry = (Entry)sender;
            //if (entry.Text.Length == 0)
            //{
            //    entry.Placeholder = "Required field.";
            //    entry.PlaceholderColor = Color.Red;
            //}
            //else
            //{
            //    entry.Placeholder = "";
            //    entry.PlaceholderColor = Color.Black;
            //}
            entry.PlaceholderColor = Color.Default;
        }
    }
}
