﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions; 
using Xamarin.Forms;

namespace Behaviors
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false);
        }

        void Submit_Clicked(object sender, System.EventArgs e)
        {
            string email = emailEntry.Text;
            //if success
            if (passwordEntry.Text == confirmPasswordEntry.Text && passwordEntry.Text.Length != 0 && confirmPasswordEntry.Text.Length != 0 && emailEntry.Text.Length != 0 && Regex.Match(email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,4})+)$").Success)
            {
                DisplayAlert("Wala na!", "Finish na! Project nata.", "Ayaw sa huhu :(");
            }
            //if no entry
            else if (passwordEntry.Text.Length == 0 && confirmPasswordEntry.Text.Length == 0 && emailEntry.Text.Length == 0)
            {
                DisplayAlert("Pungkol ka?", "Unya ra daw click ang submit. Pag input sa daw. Haaaays brain.", "Huwat oy");
                emailEntry.Placeholder = "Field is required";
                emailEntry.PlaceholderColor = Color.Red;
                passwordEntry.Placeholder = "Field is required";
                passwordEntry.PlaceholderColor = Color.Red;
                confirmPasswordEntry.Placeholder = "Field is required";
                confirmPasswordEntry.PlaceholderColor = Color.Red;

            }
            //if email entry is empty
            else if (emailEntry.Text.Length == 0 && passwordEntry.Text.Length != 0 && confirmPasswordEntry.Text.Length != 0)
            {
                DisplayAlert("HOY", "Way sud ang imong email. Sama sa imont utok! Sudli sab na.", "Lage");
                emailEntry.Placeholder = "Field is required";
                emailEntry.PlaceholderColor = Color.Red;
            }
            //email validation
            else if (!(Regex.Match(email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,4})+)$").Success))
            {
                DisplayAlert("Wrong ka", "Sayop imong email. Di kamao. Balik elementary beh!", "k.");
                emailEntry.TextColor = Color.Red;

            }
            //if password entry is empty
            else if (passwordEntry.Text.Length != 0 && confirmPasswordEntry.Text.Length == 0)
            {
                DisplayAlert("BOGO!", "I-confirm sa ayha ka mu submit. Utok bolinaw", "Oh lage");
                confirmPasswordEntry.Text = "";
                confirmPasswordEntry.Placeholder = "Field is required";
                confirmPasswordEntry.PlaceholderColor = Color.Red;
            }
            //if confirm password entry is empty
            else if (passwordEntry.Text.Length == 0 && confirmPasswordEntry.Text.Length != 0)
            {
                DisplayAlert("BOGO!", "Butangi sa ug password ayha i-confirm. Kasag bogo oy!", "Oh lage oy!");
                passwordEntry.Text = "";
                passwordEntry.Placeholder = "Field is required";
                passwordEntry.PlaceholderColor = Color.Red;

            }
            //if password entry and confirm password entry is empty
            else if (passwordEntry.Text.Length == 0 && confirmPasswordEntry.Text.Length == 0)
            {
                DisplayAlert("Hoooooy", "Wa kay password. Dali ra ka ma hack", "K");
                passwordEntry.Text = "";
                confirmPasswordEntry.Text = "";
                passwordEntry.Placeholder = "Field is required";
                passwordEntry.PlaceholderColor = Color.Red;
                confirmPasswordEntry.Placeholder = "Field is required";
                confirmPasswordEntry.PlaceholderColor = Color.Red;
            }
            //if password did not match confirm password
            else if (passwordEntry.Text != confirmPasswordEntry.Text)
            {
                DisplayAlert("Pataka!", "Tarunga na imohang password! Di parehas!", "K");
                passwordEntry.Text = "";
                confirmPasswordEntry.Text = "";
                passwordEntry.Placeholder = "Password did not match";
                passwordEntry.PlaceholderColor = Color.Red;
                confirmPasswordEntry.Placeholder = "Password did not match";
                confirmPasswordEntry.PlaceholderColor = Color.Red;
            }

        }

        void EmailEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            emailEntry.Placeholder = "";
            emailLabel.Opacity = 0;
            emailLabel.FadeTo(1, 700);
            emailLabel.IsVisible = true;
            emailEntry.TextColor = Color.Black;
        }

        void EmailEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (emailEntry.Text.Length == 0)
            {
                emailLabel.Opacity = 1;
                emailLabel.FadeTo(0, 700);
                emailLabel.IsVisible = true;
                emailEntry.Placeholder = "Email";
            }
            else
            {
                emailLabel.IsVisible = true;
            }
        }

        void PasswordEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            passwordEntry.Placeholder = "";
            passwordLabel.Opacity = 0;
            passwordLabel.FadeTo(1, 700);
            passwordLabel.IsVisible = true;
        }

        void PasswordEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (passwordEntry.Text.Length == 0)
            {
                passwordLabel.Opacity = 1;
                passwordLabel.FadeTo(0, 700);
                passwordLabel.IsVisible = true;
                passwordEntry.Placeholder = "Password";
            }
            else
            {
                passwordLabel.IsVisible = true;
            }
        }

        void ConfirmPasswordEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            confirmPasswordEntry.Placeholder = "";
            confirmPasswordLabel.Opacity = 0;
            confirmPasswordLabel.FadeTo(1, 700);
            confirmPasswordLabel.IsVisible = true;
        }

        void ConfirmPasswordEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (confirmPasswordEntry.Text.Length == 0)
            {
                confirmPasswordLabel.Opacity = 1;
                confirmPasswordLabel.FadeTo(0, 700);
                confirmPasswordLabel.IsVisible = true;
                confirmPasswordEntry.Placeholder = "Confirm Password";
            }
            else
            {
                confirmPasswordLabel.IsVisible = true;
            }
        }
    }
}
